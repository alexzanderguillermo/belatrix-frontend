import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UbigeoService {

  constructor(
    private http: HttpClient
  ) { }

  getUbigeoText() {
    const mapData = data => {
      const allTextLines = data.match(/[^\r\n]+/g);
      console.log(allTextLines);
      const rows = allTextLines.map(row => row.split('/').map(column => column.trim()));
      console.log(rows);
      
      const departamentos = [];
      const provincias = [];
      const distritos = [];

      rows.forEach(column => {
        const depData = column[0].split(" ");
        const provData = column[1].split(" ");
        const distData = column[2].split(" ");

        const depCode = depData.shift();
        const provCode = provData.shift();
        const distCode = distData.shift();

        if(!departamentos.some(dep => dep.codigo == depCode)) {
          departamentos.push({
            codigo : depCode,
            nombre : depData.join(" "),
            codigoPadre : "",
            descripcionPadre : ""  
          });
        }

        if(provCode !== "" && !provincias.some(prov => prov.codigo == provCode)) {
          provincias.push({
            codigo : provCode,
            nombre : provData.join(" "),
            codigoPadre : depCode,
            descripcionPadre : depData.join(" ") 
          });
        }
      
        if(distCode !== "" && !distritos.some(dist => dist.codigo == distCode)) {
          distritos.push({
            codigo : distCode,
            nombre : distData.join(" "),
            codigoPadre : provCode,
            descripcionPadre : provData.join(" ")  
          });
        }
        
      });

      return { departamentos, provincias, distritos };
    };

    return this.http.get('/assets/ubigeos.txt', { responseType: 'text' }).pipe(map(mapData));
  }

}
