import { Component, OnInit } from '@angular/core';
import { UbigeoService } from '../ubigeo.service';
import { ArrayType } from '@angular/compiler';

@Component({
  selector: 'app-ubigeo-list',
  templateUrl: './ubigeo-list.component.html',
  styleUrls: ['./ubigeo-list.component.css']
})
export class UbigeoListComponent implements OnInit {
  departamentos:Array<any>;
  provincias:Array<any>;
  distritos:Array<any>;

  constructor(
    private ubigeoService: UbigeoService
  ) { }

  ngOnInit() {
    this.ubigeoService.getUbigeoText().subscribe(data => {
      this.departamentos = data.departamentos;
      this.provincias = data.provincias;
      this.distritos = data.distritos;
    });

  }

}
