import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { UbigeoListComponent } from './ubigeo-list/ubigeo-list.component';
import { UbigeoTableComponent } from './ubigeo-table/ubigeo-table.component';

@NgModule({
   declarations: [
      AppComponent,
      TopBarComponent,
      UbigeoListComponent,
      UbigeoTableComponent
   ],
   imports: [
      BrowserModule,
      HttpClientModule,
      RouterModule.forRoot([
         { path: '', component: UbigeoListComponent },
       ])
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
