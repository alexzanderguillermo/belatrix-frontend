import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ubigeo-table',
  templateUrl: './ubigeo-table.component.html',
  styleUrls: ['./ubigeo-table.component.css']
})
export class UbigeoTableComponent implements OnInit {

  @Input() title:string;
  @Input() items:Array<any>;

  constructor() { }

  ngOnInit() {
  }

}
